using System;
using MonoTouch.UIKit;

namespace PopoverViewSharp
{
	public interface IPopoverViewDelegate
	{
		void PopoverViewDidSelectItemAtIndex(PopoverView popoverView, int index);		
	
		void PopoverViewDidDismiss(PopoverView popoverView);		
	}	

}
